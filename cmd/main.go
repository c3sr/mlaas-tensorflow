package main

import (
	service "bitbucket.org/c3sr/mlaas-tensorflow/service"
	config "bitbucket.org/c3sr/p3sr-config"
	log "bitbucket.org/c3sr/p3sr-logger"
	tracing "bitbucket.org/c3sr/p3sr-trace"
)

func main() {
	config.Init() // initialize etcd and so forth

	// Create a new mlaas-tensor service object
	s := service.New()

	tracer, closer := tracing.New("mlaas-tf")
	defer closer.Close()
	tracing.InitGlobalTracer(tracer)

	err := s.Start()
	if err != nil {
		log.WithError(err).Fatal("Unable to start mlaas-tensorflow service")
	}

}
