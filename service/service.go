package mlasstensorflowservice

import (
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	services "bitbucket.org/c3sr/mlaas-services"
	mlaastensorflow "bitbucket.org/c3sr/mlaas-tensorflow"
	pg "bitbucket.org/c3sr/p3sr-grpc"
	log "bitbucket.org/c3sr/p3sr-logger"
	service "bitbucket.org/c3sr/p3sr-services/service"
)

type Service struct {
	*service.Base
	server *grpc.Server
}

func New() *Service {
	desc := services.NNServiceDescription
	return &Service{Base: service.NewBase(desc),
		server: pg.NewServer(desc),
	}
}

func (s *Service) Start() error {
	return service.Start(s)
}

func (s *Service) GetServer() *grpc.Server {
	return s.server
}

func (s *Service) TrainModel(c context.Context, in *services.TrainModelWithDataRequest) (*services.TfModel, error) {
	data := in.GetData()

	// retrieve data from the database
	//mlaastensorflow.TrainModel(c, userid, dataid)
	log.Info("TrainModel called")

	log.Info(len(data))

	return &services.TfModel{Pb: []byte{}, Ckpt: []byte{}, Ckptmeta: []byte{}}, nil
}

func (s *Service) Inference(ctx context.Context, in *services.ServerInferenceRequest) (*services.InferenceReply, error) {

	pb := in.GetModel().GetPb()
	ckpt := in.GetModel().GetCkpt()
	ckptmeta := in.GetModel().GetCkptmeta()
	data := in.GetData()

	log.Info(len(pb), " ", len(data), " ", len(ckpt), " ", len(ckptmeta))

	class, ctx, err := mlaastensorflow.Inference(ctx, data, pb, ckpt, ckptmeta)

	return &services.InferenceReply{Class: class}, err
}
