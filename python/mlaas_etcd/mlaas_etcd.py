import etcd
from multiprocessing import Pool
from threading import Thread
import time

MLAAS_SERVICE_DIR = "/mlaasservices"
MLAAS_DB_KEY = 'Db'
MLAAS_NN_KEY = 'Nn'

ETCD_TIMEOUT = 10
ETCD_HOST = 'etcd.p3sr.com'
ETCD_PORT = 2379


class Client(object):

    def __init__(self):
        self.etcd_client = etcd.Client(host=ETCD_HOST, port=ETCD_PORT)

    def FindMlaasDbService(self):
        path = MLAAS_SERVICE_DIR + "/" + MLAAS_DB_KEY
        addr = None
        print "Trying to find mlaas server service"
        try:
            addr = self.etcd_client.get(path).value
        except etcd.EtcdKeyNotFound:
            print "Key not found at", path

        return addr

    def heartbeat(self, key, value):
        class HeartbeatThread(Thread):

            def __init__(self, key, value):
                self.stopped = False
                self.etcd_client = etcd.Client(host=ETCD_HOST, port=ETCD_PORT)
                Thread.__init__(self)

            def run(self):
                while not self.stopped:
                    try:
                        self.etcd_client.write(key, value, ttl=ETCD_TIMEOUT)
                        print "Updated etcd"
                    except etcd.EtcdConnectionFailed, exception:
                        print "Connection to etcd failed"
                        print exception

                    time.sleep(ETCD_TIMEOUT / 2)  # a bit hacky

        heartbeat_thread = HeartbeatThread(
            self.etcd_client.host, self.etcd_client.port)
        heartbeat_thread.daemon = True
        heartbeat_thread.start()

if __name__ == '__main__':
    client = Client()
    print client.FindMlaasDbService()
    print client.heartbeat(MLAAS_SERVICE_DIR + "/" + MLAAS_NN_KEY, "test_payload")
