# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: mlaas_services.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='mlaas_services.proto',
  package='mlaasservices',
  syntax='proto3',
  serialized_pb=_b('\n\x14mlaas_services.proto\x12\rmlaasservices\"!\n\x11\x42inaryDataMessage\x12\x0c\n\x04\x64\x61ta\x18\x01 \x01(\x0c\"0\n\x10StoreDataRequest\x12\x0e\n\x06userid\x18\x01 \x01(\t\x12\x0c\n\x04\x64\x61ta\x18\x02 \x01(\x0c\"\x1c\n\x0eStoreDataReply\x12\n\n\x02id\x18\x01 \x01(\t\"0\n\x0eGetDataRequest\x12\x0e\n\x06userid\x18\x01 \x01(\t\x12\x0e\n\x06\x64\x61taid\x18\x02 \x01(\t\"2\n\x11StoreModelRequest\x12\x0e\n\x06userid\x18\x01 \x01(\t\x12\r\n\x05model\x18\x02 \x01(\x0c\"\"\n\x0fStoreModelReply\x12\x0f\n\x07modelid\x18\x01 \x01(\t\"2\n\x0fGetModelRequest\x12\x0e\n\x06userid\x18\x01 \x01(\t\x12\x0f\n\x07modelid\x18\x02 \x01(\t\"3\n\x11TrainModelRequest\x12\x0e\n\x06userid\x18\x01 \x01(\t\x12\x0e\n\x06\x64\x61taid\x18\x02 \x01(\t\")\n\x19TrainModelWithDataRequest\x12\x0c\n\x04\x64\x61ta\x18\x01 \x01(\x0c\"\"\n\x0fTrainModelReply\x12\x0f\n\x07modelid\x18\x01 \x01(\t\"A\n\x10InferenceRequest\x12\x0e\n\x06userid\x18\x01 \x01(\t\x12\x0f\n\x07modelid\x18\x02 \x01(\t\x12\x0c\n\x04\x64\x61ta\x18\x03 \x01(\x0c\"\x1f\n\x0eInferenceReply\x12\r\n\x05\x63lass\x18\x01 \x01(\t2\xca\x02\n\tDbService\x12M\n\tStoreData\x12\x1f.mlaasservices.StoreDataRequest\x1a\x1d.mlaasservices.StoreDataReply\"\x00\x12L\n\x07GetData\x12\x1d.mlaasservices.GetDataRequest\x1a .mlaasservices.BinaryDataMessage\"\x00\x12P\n\nStoreModel\x12 .mlaasservices.StoreModelRequest\x1a\x1e.mlaasservices.StoreModelReply\"\x00\x12N\n\x08GetModel\x12\x1e.mlaasservices.GetModelRequest\x1a .mlaasservices.BinaryDataMessage\"\x00\x32\xb6\x01\n\tNNService\x12Z\n\nTrainModel\x12(.mlaasservices.TrainModelWithDataRequest\x1a .mlaasservices.BinaryDataMessage\"\x00\x12M\n\tInference\x12\x1f.mlaasservices.InferenceRequest\x1a\x1d.mlaasservices.InferenceReply\"\x00\x32\xff\x01\n\rServerService\x12M\n\tStoreData\x12\x1f.mlaasservices.StoreDataRequest\x1a\x1d.mlaasservices.StoreDataReply\"\x00\x12P\n\nTrainModel\x12 .mlaasservices.TrainModelRequest\x1a\x1e.mlaasservices.TrainModelReply\"\x00\x12M\n\tInference\x12\x1f.mlaasservices.InferenceRequest\x1a\x1d.mlaasservices.InferenceReply\"\x00\x62\x06proto3')
)
_sym_db.RegisterFileDescriptor(DESCRIPTOR)




_BINARYDATAMESSAGE = _descriptor.Descriptor(
  name='BinaryDataMessage',
  full_name='mlaasservices.BinaryDataMessage',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='data', full_name='mlaasservices.BinaryDataMessage.data', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=39,
  serialized_end=72,
)


_STOREDATAREQUEST = _descriptor.Descriptor(
  name='StoreDataRequest',
  full_name='mlaasservices.StoreDataRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='userid', full_name='mlaasservices.StoreDataRequest.userid', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='data', full_name='mlaasservices.StoreDataRequest.data', index=1,
      number=2, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=74,
  serialized_end=122,
)


_STOREDATAREPLY = _descriptor.Descriptor(
  name='StoreDataReply',
  full_name='mlaasservices.StoreDataReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='mlaasservices.StoreDataReply.id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=124,
  serialized_end=152,
)


_GETDATAREQUEST = _descriptor.Descriptor(
  name='GetDataRequest',
  full_name='mlaasservices.GetDataRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='userid', full_name='mlaasservices.GetDataRequest.userid', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='dataid', full_name='mlaasservices.GetDataRequest.dataid', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=154,
  serialized_end=202,
)


_STOREMODELREQUEST = _descriptor.Descriptor(
  name='StoreModelRequest',
  full_name='mlaasservices.StoreModelRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='userid', full_name='mlaasservices.StoreModelRequest.userid', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='model', full_name='mlaasservices.StoreModelRequest.model', index=1,
      number=2, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=204,
  serialized_end=254,
)


_STOREMODELREPLY = _descriptor.Descriptor(
  name='StoreModelReply',
  full_name='mlaasservices.StoreModelReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='modelid', full_name='mlaasservices.StoreModelReply.modelid', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=256,
  serialized_end=290,
)


_GETMODELREQUEST = _descriptor.Descriptor(
  name='GetModelRequest',
  full_name='mlaasservices.GetModelRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='userid', full_name='mlaasservices.GetModelRequest.userid', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='modelid', full_name='mlaasservices.GetModelRequest.modelid', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=292,
  serialized_end=342,
)


_TRAINMODELREQUEST = _descriptor.Descriptor(
  name='TrainModelRequest',
  full_name='mlaasservices.TrainModelRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='userid', full_name='mlaasservices.TrainModelRequest.userid', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='dataid', full_name='mlaasservices.TrainModelRequest.dataid', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=344,
  serialized_end=395,
)


_TRAINMODELWITHDATAREQUEST = _descriptor.Descriptor(
  name='TrainModelWithDataRequest',
  full_name='mlaasservices.TrainModelWithDataRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='data', full_name='mlaasservices.TrainModelWithDataRequest.data', index=0,
      number=1, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=397,
  serialized_end=438,
)


_TRAINMODELREPLY = _descriptor.Descriptor(
  name='TrainModelReply',
  full_name='mlaasservices.TrainModelReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='modelid', full_name='mlaasservices.TrainModelReply.modelid', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=440,
  serialized_end=474,
)


_INFERENCEREQUEST = _descriptor.Descriptor(
  name='InferenceRequest',
  full_name='mlaasservices.InferenceRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='userid', full_name='mlaasservices.InferenceRequest.userid', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='modelid', full_name='mlaasservices.InferenceRequest.modelid', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='data', full_name='mlaasservices.InferenceRequest.data', index=2,
      number=3, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=476,
  serialized_end=541,
)


_INFERENCEREPLY = _descriptor.Descriptor(
  name='InferenceReply',
  full_name='mlaasservices.InferenceReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='class', full_name='mlaasservices.InferenceReply.class', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=543,
  serialized_end=574,
)

DESCRIPTOR.message_types_by_name['BinaryDataMessage'] = _BINARYDATAMESSAGE
DESCRIPTOR.message_types_by_name['StoreDataRequest'] = _STOREDATAREQUEST
DESCRIPTOR.message_types_by_name['StoreDataReply'] = _STOREDATAREPLY
DESCRIPTOR.message_types_by_name['GetDataRequest'] = _GETDATAREQUEST
DESCRIPTOR.message_types_by_name['StoreModelRequest'] = _STOREMODELREQUEST
DESCRIPTOR.message_types_by_name['StoreModelReply'] = _STOREMODELREPLY
DESCRIPTOR.message_types_by_name['GetModelRequest'] = _GETMODELREQUEST
DESCRIPTOR.message_types_by_name['TrainModelRequest'] = _TRAINMODELREQUEST
DESCRIPTOR.message_types_by_name['TrainModelWithDataRequest'] = _TRAINMODELWITHDATAREQUEST
DESCRIPTOR.message_types_by_name['TrainModelReply'] = _TRAINMODELREPLY
DESCRIPTOR.message_types_by_name['InferenceRequest'] = _INFERENCEREQUEST
DESCRIPTOR.message_types_by_name['InferenceReply'] = _INFERENCEREPLY

BinaryDataMessage = _reflection.GeneratedProtocolMessageType('BinaryDataMessage', (_message.Message,), dict(
  DESCRIPTOR = _BINARYDATAMESSAGE,
  __module__ = 'mlaas_services_pb2'
  # @@protoc_insertion_point(class_scope:mlaasservices.BinaryDataMessage)
  ))
_sym_db.RegisterMessage(BinaryDataMessage)

StoreDataRequest = _reflection.GeneratedProtocolMessageType('StoreDataRequest', (_message.Message,), dict(
  DESCRIPTOR = _STOREDATAREQUEST,
  __module__ = 'mlaas_services_pb2'
  # @@protoc_insertion_point(class_scope:mlaasservices.StoreDataRequest)
  ))
_sym_db.RegisterMessage(StoreDataRequest)

StoreDataReply = _reflection.GeneratedProtocolMessageType('StoreDataReply', (_message.Message,), dict(
  DESCRIPTOR = _STOREDATAREPLY,
  __module__ = 'mlaas_services_pb2'
  # @@protoc_insertion_point(class_scope:mlaasservices.StoreDataReply)
  ))
_sym_db.RegisterMessage(StoreDataReply)

GetDataRequest = _reflection.GeneratedProtocolMessageType('GetDataRequest', (_message.Message,), dict(
  DESCRIPTOR = _GETDATAREQUEST,
  __module__ = 'mlaas_services_pb2'
  # @@protoc_insertion_point(class_scope:mlaasservices.GetDataRequest)
  ))
_sym_db.RegisterMessage(GetDataRequest)

StoreModelRequest = _reflection.GeneratedProtocolMessageType('StoreModelRequest', (_message.Message,), dict(
  DESCRIPTOR = _STOREMODELREQUEST,
  __module__ = 'mlaas_services_pb2'
  # @@protoc_insertion_point(class_scope:mlaasservices.StoreModelRequest)
  ))
_sym_db.RegisterMessage(StoreModelRequest)

StoreModelReply = _reflection.GeneratedProtocolMessageType('StoreModelReply', (_message.Message,), dict(
  DESCRIPTOR = _STOREMODELREPLY,
  __module__ = 'mlaas_services_pb2'
  # @@protoc_insertion_point(class_scope:mlaasservices.StoreModelReply)
  ))
_sym_db.RegisterMessage(StoreModelReply)

GetModelRequest = _reflection.GeneratedProtocolMessageType('GetModelRequest', (_message.Message,), dict(
  DESCRIPTOR = _GETMODELREQUEST,
  __module__ = 'mlaas_services_pb2'
  # @@protoc_insertion_point(class_scope:mlaasservices.GetModelRequest)
  ))
_sym_db.RegisterMessage(GetModelRequest)

TrainModelRequest = _reflection.GeneratedProtocolMessageType('TrainModelRequest', (_message.Message,), dict(
  DESCRIPTOR = _TRAINMODELREQUEST,
  __module__ = 'mlaas_services_pb2'
  # @@protoc_insertion_point(class_scope:mlaasservices.TrainModelRequest)
  ))
_sym_db.RegisterMessage(TrainModelRequest)

TrainModelWithDataRequest = _reflection.GeneratedProtocolMessageType('TrainModelWithDataRequest', (_message.Message,), dict(
  DESCRIPTOR = _TRAINMODELWITHDATAREQUEST,
  __module__ = 'mlaas_services_pb2'
  # @@protoc_insertion_point(class_scope:mlaasservices.TrainModelWithDataRequest)
  ))
_sym_db.RegisterMessage(TrainModelWithDataRequest)

TrainModelReply = _reflection.GeneratedProtocolMessageType('TrainModelReply', (_message.Message,), dict(
  DESCRIPTOR = _TRAINMODELREPLY,
  __module__ = 'mlaas_services_pb2'
  # @@protoc_insertion_point(class_scope:mlaasservices.TrainModelReply)
  ))
_sym_db.RegisterMessage(TrainModelReply)

InferenceRequest = _reflection.GeneratedProtocolMessageType('InferenceRequest', (_message.Message,), dict(
  DESCRIPTOR = _INFERENCEREQUEST,
  __module__ = 'mlaas_services_pb2'
  # @@protoc_insertion_point(class_scope:mlaasservices.InferenceRequest)
  ))
_sym_db.RegisterMessage(InferenceRequest)

InferenceReply = _reflection.GeneratedProtocolMessageType('InferenceReply', (_message.Message,), dict(
  DESCRIPTOR = _INFERENCEREPLY,
  __module__ = 'mlaas_services_pb2'
  # @@protoc_insertion_point(class_scope:mlaasservices.InferenceReply)
  ))
_sym_db.RegisterMessage(InferenceReply)


import grpc
from grpc.beta import implementations as beta_implementations
from grpc.beta import interfaces as beta_interfaces
from grpc.framework.common import cardinality
from grpc.framework.interfaces.face import utilities as face_utilities


class DbServiceStub(object):

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.StoreData = channel.unary_unary(
        '/mlaasservices.DbService/StoreData',
        request_serializer=StoreDataRequest.SerializeToString,
        response_deserializer=StoreDataReply.FromString,
        )
    self.GetData = channel.unary_unary(
        '/mlaasservices.DbService/GetData',
        request_serializer=GetDataRequest.SerializeToString,
        response_deserializer=BinaryDataMessage.FromString,
        )
    self.StoreModel = channel.unary_unary(
        '/mlaasservices.DbService/StoreModel',
        request_serializer=StoreModelRequest.SerializeToString,
        response_deserializer=StoreModelReply.FromString,
        )
    self.GetModel = channel.unary_unary(
        '/mlaasservices.DbService/GetModel',
        request_serializer=GetModelRequest.SerializeToString,
        response_deserializer=BinaryDataMessage.FromString,
        )


class DbServiceServicer(object):

  def StoreData(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetData(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def StoreModel(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def GetModel(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_DbServiceServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'StoreData': grpc.unary_unary_rpc_method_handler(
          servicer.StoreData,
          request_deserializer=StoreDataRequest.FromString,
          response_serializer=StoreDataReply.SerializeToString,
      ),
      'GetData': grpc.unary_unary_rpc_method_handler(
          servicer.GetData,
          request_deserializer=GetDataRequest.FromString,
          response_serializer=BinaryDataMessage.SerializeToString,
      ),
      'StoreModel': grpc.unary_unary_rpc_method_handler(
          servicer.StoreModel,
          request_deserializer=StoreModelRequest.FromString,
          response_serializer=StoreModelReply.SerializeToString,
      ),
      'GetModel': grpc.unary_unary_rpc_method_handler(
          servicer.GetModel,
          request_deserializer=GetModelRequest.FromString,
          response_serializer=BinaryDataMessage.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'mlaasservices.DbService', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))


class BetaDbServiceServicer(object):
  def StoreData(self, request, context):
    context.code(beta_interfaces.StatusCode.UNIMPLEMENTED)
  def GetData(self, request, context):
    context.code(beta_interfaces.StatusCode.UNIMPLEMENTED)
  def StoreModel(self, request, context):
    context.code(beta_interfaces.StatusCode.UNIMPLEMENTED)
  def GetModel(self, request, context):
    context.code(beta_interfaces.StatusCode.UNIMPLEMENTED)


class BetaDbServiceStub(object):
  def StoreData(self, request, timeout, metadata=None, with_call=False, protocol_options=None):
    raise NotImplementedError()
  StoreData.future = None
  def GetData(self, request, timeout, metadata=None, with_call=False, protocol_options=None):
    raise NotImplementedError()
  GetData.future = None
  def StoreModel(self, request, timeout, metadata=None, with_call=False, protocol_options=None):
    raise NotImplementedError()
  StoreModel.future = None
  def GetModel(self, request, timeout, metadata=None, with_call=False, protocol_options=None):
    raise NotImplementedError()
  GetModel.future = None


def beta_create_DbService_server(servicer, pool=None, pool_size=None, default_timeout=None, maximum_timeout=None):
  request_deserializers = {
    ('mlaasservices.DbService', 'GetData'): GetDataRequest.FromString,
    ('mlaasservices.DbService', 'GetModel'): GetModelRequest.FromString,
    ('mlaasservices.DbService', 'StoreData'): StoreDataRequest.FromString,
    ('mlaasservices.DbService', 'StoreModel'): StoreModelRequest.FromString,
  }
  response_serializers = {
    ('mlaasservices.DbService', 'GetData'): BinaryDataMessage.SerializeToString,
    ('mlaasservices.DbService', 'GetModel'): BinaryDataMessage.SerializeToString,
    ('mlaasservices.DbService', 'StoreData'): StoreDataReply.SerializeToString,
    ('mlaasservices.DbService', 'StoreModel'): StoreModelReply.SerializeToString,
  }
  method_implementations = {
    ('mlaasservices.DbService', 'GetData'): face_utilities.unary_unary_inline(servicer.GetData),
    ('mlaasservices.DbService', 'GetModel'): face_utilities.unary_unary_inline(servicer.GetModel),
    ('mlaasservices.DbService', 'StoreData'): face_utilities.unary_unary_inline(servicer.StoreData),
    ('mlaasservices.DbService', 'StoreModel'): face_utilities.unary_unary_inline(servicer.StoreModel),
  }
  server_options = beta_implementations.server_options(request_deserializers=request_deserializers, response_serializers=response_serializers, thread_pool=pool, thread_pool_size=pool_size, default_timeout=default_timeout, maximum_timeout=maximum_timeout)
  return beta_implementations.server(method_implementations, options=server_options)


def beta_create_DbService_stub(channel, host=None, metadata_transformer=None, pool=None, pool_size=None):
  request_serializers = {
    ('mlaasservices.DbService', 'GetData'): GetDataRequest.SerializeToString,
    ('mlaasservices.DbService', 'GetModel'): GetModelRequest.SerializeToString,
    ('mlaasservices.DbService', 'StoreData'): StoreDataRequest.SerializeToString,
    ('mlaasservices.DbService', 'StoreModel'): StoreModelRequest.SerializeToString,
  }
  response_deserializers = {
    ('mlaasservices.DbService', 'GetData'): BinaryDataMessage.FromString,
    ('mlaasservices.DbService', 'GetModel'): BinaryDataMessage.FromString,
    ('mlaasservices.DbService', 'StoreData'): StoreDataReply.FromString,
    ('mlaasservices.DbService', 'StoreModel'): StoreModelReply.FromString,
  }
  cardinalities = {
    'GetData': cardinality.Cardinality.UNARY_UNARY,
    'GetModel': cardinality.Cardinality.UNARY_UNARY,
    'StoreData': cardinality.Cardinality.UNARY_UNARY,
    'StoreModel': cardinality.Cardinality.UNARY_UNARY,
  }
  stub_options = beta_implementations.stub_options(host=host, metadata_transformer=metadata_transformer, request_serializers=request_serializers, response_deserializers=response_deserializers, thread_pool=pool, thread_pool_size=pool_size)
  return beta_implementations.dynamic_stub(channel, 'mlaasservices.DbService', cardinalities, options=stub_options)


class NNServiceStub(object):
  """Service that has the neural network
  """

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.TrainModel = channel.unary_unary(
        '/mlaasservices.NNService/TrainModel',
        request_serializer=TrainModelWithDataRequest.SerializeToString,
        response_deserializer=BinaryDataMessage.FromString,
        )
    self.Inference = channel.unary_unary(
        '/mlaasservices.NNService/Inference',
        request_serializer=InferenceRequest.SerializeToString,
        response_deserializer=InferenceReply.FromString,
        )


class NNServiceServicer(object):
  """Service that has the neural network
  """

  def TrainModel(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Inference(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_NNServiceServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'TrainModel': grpc.unary_unary_rpc_method_handler(
          servicer.TrainModel,
          request_deserializer=TrainModelWithDataRequest.FromString,
          response_serializer=BinaryDataMessage.SerializeToString,
      ),
      'Inference': grpc.unary_unary_rpc_method_handler(
          servicer.Inference,
          request_deserializer=InferenceRequest.FromString,
          response_serializer=InferenceReply.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'mlaasservices.NNService', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))


class BetaNNServiceServicer(object):
  """Service that has the neural network
  """
  def TrainModel(self, request, context):
    context.code(beta_interfaces.StatusCode.UNIMPLEMENTED)
  def Inference(self, request, context):
    context.code(beta_interfaces.StatusCode.UNIMPLEMENTED)


class BetaNNServiceStub(object):
  """Service that has the neural network
  """
  def TrainModel(self, request, timeout, metadata=None, with_call=False, protocol_options=None):
    raise NotImplementedError()
  TrainModel.future = None
  def Inference(self, request, timeout, metadata=None, with_call=False, protocol_options=None):
    raise NotImplementedError()
  Inference.future = None


def beta_create_NNService_server(servicer, pool=None, pool_size=None, default_timeout=None, maximum_timeout=None):
  request_deserializers = {
    ('mlaasservices.NNService', 'Inference'): InferenceRequest.FromString,
    ('mlaasservices.NNService', 'TrainModel'): TrainModelWithDataRequest.FromString,
  }
  response_serializers = {
    ('mlaasservices.NNService', 'Inference'): InferenceReply.SerializeToString,
    ('mlaasservices.NNService', 'TrainModel'): BinaryDataMessage.SerializeToString,
  }
  method_implementations = {
    ('mlaasservices.NNService', 'Inference'): face_utilities.unary_unary_inline(servicer.Inference),
    ('mlaasservices.NNService', 'TrainModel'): face_utilities.unary_unary_inline(servicer.TrainModel),
  }
  server_options = beta_implementations.server_options(request_deserializers=request_deserializers, response_serializers=response_serializers, thread_pool=pool, thread_pool_size=pool_size, default_timeout=default_timeout, maximum_timeout=maximum_timeout)
  return beta_implementations.server(method_implementations, options=server_options)


def beta_create_NNService_stub(channel, host=None, metadata_transformer=None, pool=None, pool_size=None):
  request_serializers = {
    ('mlaasservices.NNService', 'Inference'): InferenceRequest.SerializeToString,
    ('mlaasservices.NNService', 'TrainModel'): TrainModelWithDataRequest.SerializeToString,
  }
  response_deserializers = {
    ('mlaasservices.NNService', 'Inference'): InferenceReply.FromString,
    ('mlaasservices.NNService', 'TrainModel'): BinaryDataMessage.FromString,
  }
  cardinalities = {
    'Inference': cardinality.Cardinality.UNARY_UNARY,
    'TrainModel': cardinality.Cardinality.UNARY_UNARY,
  }
  stub_options = beta_implementations.stub_options(host=host, metadata_transformer=metadata_transformer, request_serializers=request_serializers, response_deserializers=response_deserializers, thread_pool=pool, thread_pool_size=pool_size)
  return beta_implementations.dynamic_stub(channel, 'mlaasservices.NNService', cardinalities, options=stub_options)


class ServerServiceStub(object):
  """Service that outside clients can use
  """

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.StoreData = channel.unary_unary(
        '/mlaasservices.ServerService/StoreData',
        request_serializer=StoreDataRequest.SerializeToString,
        response_deserializer=StoreDataReply.FromString,
        )
    self.TrainModel = channel.unary_unary(
        '/mlaasservices.ServerService/TrainModel',
        request_serializer=TrainModelRequest.SerializeToString,
        response_deserializer=TrainModelReply.FromString,
        )
    self.Inference = channel.unary_unary(
        '/mlaasservices.ServerService/Inference',
        request_serializer=InferenceRequest.SerializeToString,
        response_deserializer=InferenceReply.FromString,
        )


class ServerServiceServicer(object):
  """Service that outside clients can use
  """

  def StoreData(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def TrainModel(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def Inference(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_ServerServiceServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'StoreData': grpc.unary_unary_rpc_method_handler(
          servicer.StoreData,
          request_deserializer=StoreDataRequest.FromString,
          response_serializer=StoreDataReply.SerializeToString,
      ),
      'TrainModel': grpc.unary_unary_rpc_method_handler(
          servicer.TrainModel,
          request_deserializer=TrainModelRequest.FromString,
          response_serializer=TrainModelReply.SerializeToString,
      ),
      'Inference': grpc.unary_unary_rpc_method_handler(
          servicer.Inference,
          request_deserializer=InferenceRequest.FromString,
          response_serializer=InferenceReply.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'mlaasservices.ServerService', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))


class BetaServerServiceServicer(object):
  """Service that outside clients can use
  """
  def StoreData(self, request, context):
    context.code(beta_interfaces.StatusCode.UNIMPLEMENTED)
  def TrainModel(self, request, context):
    context.code(beta_interfaces.StatusCode.UNIMPLEMENTED)
  def Inference(self, request, context):
    context.code(beta_interfaces.StatusCode.UNIMPLEMENTED)


class BetaServerServiceStub(object):
  """Service that outside clients can use
  """
  def StoreData(self, request, timeout, metadata=None, with_call=False, protocol_options=None):
    raise NotImplementedError()
  StoreData.future = None
  def TrainModel(self, request, timeout, metadata=None, with_call=False, protocol_options=None):
    raise NotImplementedError()
  TrainModel.future = None
  def Inference(self, request, timeout, metadata=None, with_call=False, protocol_options=None):
    raise NotImplementedError()
  Inference.future = None


def beta_create_ServerService_server(servicer, pool=None, pool_size=None, default_timeout=None, maximum_timeout=None):
  request_deserializers = {
    ('mlaasservices.ServerService', 'Inference'): InferenceRequest.FromString,
    ('mlaasservices.ServerService', 'StoreData'): StoreDataRequest.FromString,
    ('mlaasservices.ServerService', 'TrainModel'): TrainModelRequest.FromString,
  }
  response_serializers = {
    ('mlaasservices.ServerService', 'Inference'): InferenceReply.SerializeToString,
    ('mlaasservices.ServerService', 'StoreData'): StoreDataReply.SerializeToString,
    ('mlaasservices.ServerService', 'TrainModel'): TrainModelReply.SerializeToString,
  }
  method_implementations = {
    ('mlaasservices.ServerService', 'Inference'): face_utilities.unary_unary_inline(servicer.Inference),
    ('mlaasservices.ServerService', 'StoreData'): face_utilities.unary_unary_inline(servicer.StoreData),
    ('mlaasservices.ServerService', 'TrainModel'): face_utilities.unary_unary_inline(servicer.TrainModel),
  }
  server_options = beta_implementations.server_options(request_deserializers=request_deserializers, response_serializers=response_serializers, thread_pool=pool, thread_pool_size=pool_size, default_timeout=default_timeout, maximum_timeout=maximum_timeout)
  return beta_implementations.server(method_implementations, options=server_options)


def beta_create_ServerService_stub(channel, host=None, metadata_transformer=None, pool=None, pool_size=None):
  request_serializers = {
    ('mlaasservices.ServerService', 'Inference'): InferenceRequest.SerializeToString,
    ('mlaasservices.ServerService', 'StoreData'): StoreDataRequest.SerializeToString,
    ('mlaasservices.ServerService', 'TrainModel'): TrainModelRequest.SerializeToString,
  }
  response_deserializers = {
    ('mlaasservices.ServerService', 'Inference'): InferenceReply.FromString,
    ('mlaasservices.ServerService', 'StoreData'): StoreDataReply.FromString,
    ('mlaasservices.ServerService', 'TrainModel'): TrainModelReply.FromString,
  }
  cardinalities = {
    'Inference': cardinality.Cardinality.UNARY_UNARY,
    'StoreData': cardinality.Cardinality.UNARY_UNARY,
    'TrainModel': cardinality.Cardinality.UNARY_UNARY,
  }
  stub_options = beta_implementations.stub_options(host=host, metadata_transformer=metadata_transformer, request_serializers=request_serializers, response_deserializers=response_deserializers, thread_pool=pool, thread_pool_size=pool_size)
  return beta_implementations.dynamic_stub(channel, 'mlaasservices.ServerService', cardinalities, options=stub_options)
# @@protoc_insertion_point(module_scope)
