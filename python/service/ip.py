'''Get an available port on the system'''

import socket
import urllib2

def get_external_ip_from(service):
    s = urllib2.urlopen(service).read() 
    return s.strip()

def get_port():
    '''Return a free port as an integer'''
    sock = socket.socket()
    sock.bind(('', 0))
    port = sock.getsockname()[1]
    sock.close()
    return port


def get_local_ip():
    '''Return the local ip address'''
    host = socket.gethostbyname(socket.gethostname())

    return host

def get_external_ip():
    services = ["http://checkip.amazonaws.com",
    "http://myexternalip.com/raw",
    "http://icanhazip.com",
    "http://canihazip.com/s"]

    for service in services:
        ip = get_external_ip_from(service)
        if ip is not None:
            return ip
		
    return None

def get_new_ip_address():
    port = get_port()
    ip = get_local_ip()
    if ip is None:
        return None
    return ip + ":" + str(port)

if __name__ == '__main__':
    print "Local IP:     ", get_local_ip()
    print "External IP:  ", get_external_ip()
    print "A new address:", get_new_ip_address()