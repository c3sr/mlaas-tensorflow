from concurrent.futures import ThreadPoolExecutor
import grpc
from ip import get_external_ip, get_port
import mlaas_services_pb2
import os
import sys
import threading
import time

def _add_syspath(relpath):
#    cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
    cmd_folder = os.path.realpath(os.path.abspath(relpath))
    print "adding", cmd_folder, "to sys.path for import"
    if cmd_folder not in sys.path:
        sys.path.insert(0, cmd_folder)

_add_syspath("../mlaas_etcd")

import mlaas_etcd as etcd


# Provides an implementation of the NNServiceServer methods

class NNServiceServicer(mlaas_services_pb2.NNServiceServicer):
    def TrainModel(self, request, context):
        print "TrainModel called"
        return

    def Inference(self, request, context):
        print "Inference called"
        return

    def serve(self, port):
        server = grpc.server(ThreadPoolExecutor(max_workers=10))
        mlaas_services_pb2.add_NNServiceServicer_to_server(self, server)
        server.add_insecure_port('[::]:' + str(port))
        server.start()

class Service(NNServiceServicer):
    def __init__(self):
        self.etcd_client = etcd.Client()


    def start(self):
        # Get a free port
        ip = get_external_ip()
        port = get_port()
        address = ip + ":" + str(port)

        # Register self with etcd
        self.etcd_client.heartbeat(etcd.MLAAS_SERVICE_DIR + "/" + etcd.MLAAS_NN_KEY, address)

        # run the NNServiceServicer
        self.serve(port)
        print "Serving mlaas-nn requests on", address
        while True:
            time.sleep(1)


if __name__ == '__main__':
    s = Service()
    s.start()
    print "after start"