# README #

Building tensorflow go bindings: `https://github.com/tensorflow/tensorflow/tree/master/tensorflow/go`

To build with CUDA

    cd ${GOPATH}/src/github.com/tensorflow/tensorflow
    ./configure
    bazel build -c opt --config=cuda //tensorflow:libtensorflow.so

To build without CUDA

    cd ${GOPATH}/src/github.com/tensorflow/tensorflow
    ./configure
    bazel build -c opt //tensorflow:libtensorflow.so

Make `libtensorflow.so` availabel to the linker, either by copying to a system location or adding it to a directory in `LD_LIBRARY_PATH`.